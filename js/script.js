(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.fullpagejs = {
    attach: function (context, settings) {

      const ds = drupalSettings.fullpagejs;

      $(ds.fullPageSelector, context)
        .once("fullpagejs")
        .each(function () {

          $(ds.fullPageSelector).fullpage({
            licenseKey: ds.licenseKey,
            sectionSelector: ds.sectionSelector,
            slideSelector: ds.slideSelector,
            controlArrows: ds.controlArrows,
            verticalCentered: ds.verticalCentered,
            scrollingSpeed: ds.scrollingSpeed,
            sectionsColor: ds.sectionsColor,
            anchors: ds.anchors,
            lockAnchors: ds.lockAnchors,
            easing: ds.easing,
            easingcss3: ds.easingcss3,
            loopTop: ds.loopTop,
            loopBottom: ds.loopBottom,
            loopHorizontal: ds.loopHorizontal,
            css3: ds.css3,
            autoScrolling: ds.autoScrolling,
            fitToSection: ds.fitToSection,
            fitToSectionDelay: ds.fitToSectionDelay,
            scrollBar: ds.scrollBar,
            paddingTop: ds.paddingTop,
            paddingBottom: ds.paddingBottom,
            fixedElements: ds.fixedElements,
            normalScrollElements: ds.normalScrollElements,
            bigSectionsDestination: ds.bigSectionsDestination,
            keyboardScrolling: ds.keyboardScrolling,
            touchSensitivity: ds.touchSensitivity,
            continuousVertical: ds.continuousVertical,
            animateAnchor: ds.animateAnchor,
            recordHistory: ds.recordHistory,
            menu: ds.menu,
            navigation: ds.navigation,
            navigationPosition: ds.navigationPosition,
            navigationTooltips: ds.navigationTooltips,
            showActiveTooltip: ds.showActiveTooltip,
            slidesNavigation: ds.slidesNavigation,
            slidesNavPosition: ds.slidesNavPosition,
            scrollOverflow: ds.scrollOverflow,
            responsiveWidth: ds.responsiveWidth,
            responsiveHeight: ds.responsiveHeight,
            lazyLoading: ds.lazyLoading,
            cards: ds.cards,
            cardsKey: ds.cardsKey,
            cardsOptions: {
              perspective: ds.cardsPerspective,
              fadeContent: ds.cardsFadeContent,
              fadeBackground: ds.cardsFadeBackground
            },
            parallax: ds.parallax,
            parallaxKey: ds.parallaxKey,
            parallaxOptions: {
              type: ds.parallaxType,
              percentage: ds.parallaxPercentage,
              property: ds.parallaxProperty
            },
            scrollOverflowReset: ds.scrollOverflowReset,
            scrollOverflowResetKey: ds.scrollOverflowResetKey,
            dragAndMove: ds.dragAndMove,
            dragAndMoveKey: ds.dragAndMoveKey,
            offsetSections: ds.offsetSections,
            offsetSectionsKey: ds.offsetSectionsKey,
            fadingEffect: ds.fadingEffect,
            fadingEffectKey: ds.fadingEffectKey,
            responsiveSlides: ds.responsiveSlides,
            responsiveSlidesKey: ds.responsiveSlidesKey,
            continuousHorizontal: ds.continuousHorizontal,
            continuousHorizontalKey: ds.continuousHorizontalKey,
            interlockedSlides: ds.interlockedSlides,
            interlockedSlidesKey: ds.interlockedSlidesKey,
            scrollHorizontally: ds.scrollHorizontally,
            scrollHorizontallyKey: ds.scrollHorizontallyKey,
            resetSliders: ds.resetSliders,
            resetSlidersKey: ds.resetSlidersKey,

            //Events
            // onLeave: function (origin, destination, direction) { },
            // afterLoad: function (origin, destination, direction) { },
            // afterRender: function () { },
            // afterResize: function (width, height) { },
            // afterReBuild: function () { },
            // afterResponsive: function (isResponsive) { },
            // afterSlideLoad: function (section, origin, destination, direction) { },
            // onSlideLeave: function (section, origin, destination, direction) { }
          });
        });
    },
  };
})(jQuery, Drupal, drupalSettings);
