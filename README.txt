This is a Drupal module that makes it possible to use the JavaScript plugin fullPage.js

  - Access page module settings: /admin/config/user-interface/fullpagejs
  - Visit plugin documentation page: https://github.com/alvarotrigo/fullPage.js

To use the fullPage.js extensions, you need to include the extension files directly in your project theme.

For example, to use the Parallax Backgrounds and ScrollOverflow Reset extensions, do the following:

1 - Add on your my_theme.libraries.yml an entry for a new library, example:

  fullpagejs-extensions:
    remote: https://github.com/alvarotrigo/fullPage.js/
    version: 3.0.8
    license:
      name: GPLv3
      remote: https://github.com/alvarotrigo/fullPage.js/blob/master/LICENSE
      gpl-compatible: true
    js:
      js/fullpage.parallax.min.js: { weight: -3 }
      https://unpkg.com/fullpage.js/vendors/scrolloverflow.min.js: { weight: -5 }
      js/fullpage.scrollOverflowReset.min.js: { weight: -4 }
    dependencies:
      - fullpagejs/fullpagejs

  - You need to add inside js: the extension files.
  - Note the negative weights to define the loading order of the extensions.

2 - Add the files for each extension in your theme folder, for example:

  /themes/my_theme/js/fullpage.parallax.min.js
  /themes/my_theme/js/fullpage.scrollOverflowReset.min.js

3 - Create a my_theme.theme file in your theme folder.

4 - Add in your my_theme.theme file, a Drupal hook that will attach the extension files to the project. For example:

  <?php

  /**
  * Implements hook_preprocess_page().
  */
  function my_theme_preprocess_page(&$variables) {
    // Check if is admin route.
    $is_admin = \Drupal::service('router.admin_context')->isAdminRoute();

    // Get current node.
    $node = \Drupal::request()->attributes->get('node');

    // Get all content types saved on fullpagejs config.
    $content_types =  \Drupal::config('fullpagejs.settings')->get('drupalContentTypes');

    if(!$is_admin && $node && $content_types) {

      // Get node content type.
      $node_type = strval($node->getType());

      if($node_type === $content_types[$node_type]) {

        $variables['#attached']['library'][] = 'my_theme/fullpagejs-extensions';
      }
    }
  }

  5 - Clear Drupal cache.
