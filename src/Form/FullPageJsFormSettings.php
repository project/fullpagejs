<?php

namespace Drupal\fullpagejs\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\NodeType;

/**
 * Class FullPageJsFormSettings.
 */
class FullPageJsFormSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'fullpagejs.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fullpagejs_form_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('fullpagejs.settings');

    $form['informations'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('For details informations about each plugin options, please visit: https://github.com/alvarotrigo/fullPage.js'),
    ];

    // Load all content types available.
    $node_types = NodeType::loadMultiple();

    // Create options with all content types.
    $node_type_options = [];
    foreach ($node_types as $node_type) {
      $node_type_options[$node_type->id()] = $node_type->label();
    }

    $form['drupalContentTypes'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Content types'),
      '#options' => $node_type_options,
      '#default_value' => $config->get('drupalContentTypes') ?: [],
      '#description' => $this->t('Select which content types the plugin should be added to.'),
    ];

    $form['licenseKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('licenseKey'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $config->get('licenseKey'),
    ];

    $form['fullPageSelector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('fullPageSelector'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $config->get('fullPageSelector'),
      '#description' => $this->t('Default: #fullpage'),
    ];

    $form['sectionSelector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('sectionSelector'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $config->get('sectionSelector'),
    ];

    $form['slideSelector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('slideSelector'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $config->get('slideSelector'),
    ];

    $form['controlArrows'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('controlArrows'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('controlArrows'),
    ];

    $form['verticalCentered'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('verticalCentered'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('verticalCentered'),
    ];

    $form['scrollingSpeed'] = [
      '#type' => 'number',
      '#title' => $this->t('scrollingSpeed'),
      '#size' => 30,
      '#default_value' => $config->get('scrollingSpeed'),
    ];

    $form['sectionsColor'] = [
      '#type' => 'textfield',
      '#title' => $this->t('sectionsColor'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $config->get('sectionsColor'),
      '#description' => $this->t('Enter values separated by comma. <br>Ex: #f2f2f2, #4BBFC3, #7BAABE, whitesmoke, #000'),
    ];

    $form['anchors'] = [
      '#type' => 'textfield',
      '#title' => $this->t('anchors'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $config->get('anchors'),
      '#description' => $this->t('Enter values separated by comma. <br>Ex: firstPage, secondPage, thirdPage, lastPage'),
    ];

    $form['lockAnchors'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('lockAnchors'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('lockAnchors'),
    ];

    $form['easing'] = [
      '#type' => 'textfield',
      '#title' => $this->t('easing'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $config->get('easing'),
    ];

    $form['easingcss3'] = [
      '#type' => 'textfield',
      '#title' => $this->t('easingcss3'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $config->get('easingcss3'),
    ];

    $form['loopTop'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('loopTop'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('loopTop'),
    ];

    $form['loopBottom'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('loopBottom'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('loopBottom'),
    ];

    $form['loopHorizontal'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('loopHorizontal'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('loopHorizontal'),
    ];

    $form['css3'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('css3'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('css3'),
    ];

    $form['autoScrolling'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('autoScrolling'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('autoScrolling'),
    ];

    $form['fitToSection'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('fitToSection'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('fitToSection'),
    ];

    $form['fitToSectionDelay'] = [
      '#type' => 'number',
      '#title' => $this->t('fitToSectionDelay'),
      '#size' => 30,
      '#default_value' => $config->get('fitToSectionDelay'),
    ];

    $form['scrollBar'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('scrollBar'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('scrollBar'),
    ];

    $form['paddingTop'] = [
      '#type' => 'textfield',
      '#title' => $this->t('paddingTop'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $config->get('paddingTop'),
      '#description' => $this->t('Ex: 10px or 10em<br> Leave empty for 0'),
    ];

    $form['paddingBottom'] = [
      '#type' => 'textfield',
      '#title' => $this->t('paddingBottom'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $config->get('paddingBottom'),
      '#description' => $this->t('Ex: 10px or 10em<br> Leave empty for 0'),
    ];

    $form['fixedElements'] = [
      '#type' => 'textfield',
      '#title' => $this->t('fixedElements'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $config->get('fixedElements'),
      '#description' => $this->t('Enter values separated by comma. <br>Ex: #element1, .element2'),
    ];

    $form['normalScrollElements'] = [
      '#type' => 'textfield',
      '#title' => $this->t('normalScrollElements'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $config->get('normalScrollElements'),
      '#description' => $this->t('Enter values separated by comma. <br>Ex: #element1, .element2'),
    ];

    $form['bigSectionsDestination'] = [
      '#type' => 'select',
      '#title' => $this->t('bigSectionsDestination'),
      '#options' => [
        'null' => $this->t('null'),
        'top' => $this->t('top'),
        'bottom' => $this->t('bottom'),
      ],
      '#default_value' => $config->get('bigSectionsDestination'),
    ];

    $form['keyboardScrolling'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('keyboardScrolling'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('keyboardScrolling'),
    ];

    $form['touchSensitivity'] = [
      '#type' => 'number',
      '#title' => $this->t('touchSensitivity'),
      '#min' => 0,
      '#max' => 100,
      '#size' => 30,
      '#default_value' => $config->get('touchSensitivity'),
    ];

    $form['continuousVertical'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('continuousVertical'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('continuousVertical'),
    ];

    $form['animateAnchor'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('animateAnchor'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('animateAnchor'),
    ];

    $form['recordHistory'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('recordHistory'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('recordHistory'),
    ];

    $form['menu'] = [
      '#type' => 'textfield',
      '#title' => $this->t('menu'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $config->get('menu'),
      '#description' => $this->t('A selector can be used to specify the menu to link with the sections. Leave empty for default false.'),
    ];

    $form['navigation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('navigation'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('navigation'),
    ];

    $form['navigationPosition'] = [
      '#type' => 'select',
      '#title' => $this->t('navigationPosition'),
      '#options' => [
        'none' => $this->t('none'),
        'left' => $this->t('left'),
        'right' => $this->t('right'),
      ],
      '#default_value' => $config->get('navigationPosition'),
    ];

    $form['navigationTooltips'] = [
      '#type' => 'textfield',
      '#title' => $this->t('navigationTooltips'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $config->get('navigationTooltips'),
      '#description' => $this->t('Enter values separated by comma. <br>Ex: firstSlide, secondSlide'),
    ];

    $form['showActiveTooltip'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('showActiveTooltip'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('showActiveTooltip'),
    ];

    $form['slidesNavigation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('slidesNavigation'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('slidesNavigation'),
    ];

    $form['slidesNavPosition'] = [
      '#type' => 'select',
      '#title' => $this->t('slidesNavPosition'),
      '#options' => [
        'bottom' => $this->t('bottom'),
        'top' => $this->t('top'),
      ],
      '#default_value' => $config->get('slidesNavPosition'),
    ];

    $form['scrollOverflow'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('scrollOverflow'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('scrollOverflow'),
    ];

    $form['responsiveWidth'] = [
      '#type' => 'number',
      '#title' => $this->t('responsiveWidth'),
      '#size' => 30,
      '#default_value' => $config->get('responsiveWidth'),
    ];

    $form['responsiveHeight'] = [
      '#type' => 'number',
      '#title' => $this->t('responsiveHeight'),
      '#size' => 30,
      '#default_value' => $config->get('responsiveHeight'),
    ];

    $form['lazyLoading'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('lazyLoading'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('lazyLoading'),
    ];

    $form['cards'] = [
      '#type' => 'details',
      '#title' => $this->t('Cards 3d'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#open' => FALSE,
      '#tree' => TRUE,
    ];

    $form['cards']['cardsKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('cardsKey'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $config->get('cardsKey'),
    ];

    $form['cards']['cards'] = [
      '#type' => 'select',
      '#title' => $this->t('cards'),
      '#options' => [
        'true' => $this->t('true'),
        'false' => $this->t('false'),
        'sections' => $this->t('sections'),
        'slides' => $this->t('slides'),
      ],
      '#default_value' => $config->get('cards'),
    ];

    $form['cards']['cardsPerspective'] = [
      '#type' => 'number',
      '#title' => $this->t('perspective'),
      '#size' => 30,
      '#default_value' => $config->get('cardsPerspective'),
    ];

    $form['cards']['cardsFadeContent'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('fadeContent'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('cardsFadeContent'),
    ];

    $form['cards']['cardsFadeBackground'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('fadeBackground'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('cardsFadeBackground'),
    ];

    $form['parallax'] = [
      '#type' => 'details',
      '#title' => $this->t('Parallax Backgrounds'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#open' => FALSE,
      '#tree' => TRUE,
    ];

    $form['parallax']['parallaxKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('parallaxKey'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $config->get('parallaxKey'),
    ];

    $form['parallax']['parallax'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('parallax'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('parallax'),
    ];

    $form['parallax']['parallaxType'] = [
      '#type' => 'select',
      '#title' => $this->t('type'),
      '#options' => [
        'reveal' => $this->t('reveal'),
        'cover' => $this->t('cover'),
      ],
      '#default_value' => $config->get('parallaxType'),
    ];

    $form['parallax']['parallaxPercentage'] = [
      '#type' => 'number',
      '#title' => $this->t('percentage'),
      '#min' => 0,
      '#max' => 100,
      '#size' => 30,
      '#field_suffix' => '%',
      '#default_value' => $config->get('parallaxPercentage'),
    ];

    $form['parallax']['parallaxProperty'] = [
      '#type' => 'select',
      '#title' => $this->t('property'),
      '#options' => [
        'translate' => $this->t('translate'),
        'background' => $this->t('background'),
      ],
      '#default_value' => $config->get('parallaxProperty'),
    ];

    $form['scrollOverflowReset'] = [
      '#type' => 'details',
      '#title' => $this->t('ScrollOverflow Reset'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#open' => FALSE,
      '#tree' => TRUE,
    ];

    $form['scrollOverflowReset']['scrollOverflowResetKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('scrollOverflowResetKey'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $config->get('scrollOverflowResetKey'),
    ];

    $form['scrollOverflowReset']['scrollOverflowReset'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('scrollOverflowReset'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('scrollOverflowReset'),
    ];

    $form['dragAndMove'] = [
      '#type' => 'details',
      '#title' => $this->t('Drag And Move'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#open' => FALSE,
      '#tree' => TRUE,
    ];

    $form['dragAndMove']['dragAndMoveKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('dragAndMoveKey'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $config->get('dragAndMoveKey'),
    ];

    $form['dragAndMove']['dragAndMove'] = [
      '#type' => 'select',
      '#title' => $this->t('dragAndMove'),
      '#options' => [
        'true' => $this->t('true'),
        'false' => $this->t('false'),
        'vertical' => $this->t('vertical'),
        'horizontal' => $this->t('horizontal'),
        'fingersonly' => $this->t('fingersonly'),
        'mouseonly' => $this->t('mouseonly'),
      ],
      '#default_value' => $config->get('dragAndMove'),
    ];

    $form['offsetSections'] = [
      '#type' => 'details',
      '#title' => $this->t('Offset Sections'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#open' => FALSE,
      '#tree' => TRUE,
    ];

    $form['offsetSections']['offsetSectionsKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('offsetSectionsKey'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $config->get('offsetSectionsKey'),
    ];

    $form['offsetSections']['offsetSections'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('offsetSections'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('offsetSections'),
    ];

    $form['fadingEffect'] = [
      '#type' => 'details',
      '#title' => $this->t('Fading Effect'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#open' => FALSE,
      '#tree' => TRUE,
    ];

    $form['fadingEffect']['fadingEffectKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('fadingEffectKey'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $config->get('fadingEffectKey'),
    ];

    $form['fadingEffect']['fadingEffect'] = [
      '#type' => 'select',
      '#title' => $this->t('fadingEffect'),
      '#options' => [
        'true' => $this->t('true'),
        'false' => $this->t('false'),
        'sections' => $this->t('sections'),
        'slides' => $this->t('slides'),
      ],
      '#default_value' => $config->get('fadingEffect'),
    ];

    $form['responsiveSlides'] = [
      '#type' => 'details',
      '#title' => $this->t('Responsive Slides'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#open' => FALSE,
      '#tree' => TRUE,
    ];

    $form['responsiveSlides']['responsiveSlidesKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('responsiveSlidesKey'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $config->get('responsiveSlidesKey'),
    ];

    $form['responsiveSlides']['responsiveSlides'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('responsiveSlides'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('responsiveSlides'),
    ];

    $form['continuousHorizontal'] = [
      '#type' => 'details',
      '#title' => $this->t('Continuous Horizontal'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#open' => FALSE,
      '#tree' => TRUE,
    ];

    $form['continuousHorizontal']['continuousHorizontalKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('continuousHorizontalKey'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $config->get('continuousHorizontalKey'),
    ];

    $form['continuousHorizontal']['continuousHorizontal'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('continuousHorizontal'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('continuousHorizontal'),
    ];

    $form['interlockedSlides'] = [
      '#type' => 'details',
      '#title' => $this->t('Interlocked Slides'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#open' => FALSE,
      '#tree' => TRUE,
    ];

    $form['interlockedSlides']['interlockedSlidesKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('interlockedSlidesKey'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $config->get('interlockedSlidesKey'),
    ];

    $form['interlockedSlides']['interlockedSlides'] = [
      '#type' => 'textfield',
      '#title' => $this->t('interlockedSlides'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $config->get('interlockedSlides'),
      '#description' => $this->t('Enter true, false or numbers separated by comma. <br> Ex: 1,3 <br>Default: false'),
    ];

    $form['scrollHorizontally'] = [
      '#type' => 'details',
      '#title' => $this->t('Scroll Horizontally'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#open' => FALSE,
      '#tree' => TRUE,
    ];

    $form['scrollHorizontally']['scrollHorizontallyKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('scrollHorizontallyKey'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $config->get('scrollHorizontallyKey'),
    ];

    $form['scrollHorizontally']['scrollHorizontally'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('scrollHorizontally'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('scrollHorizontally'),
    ];

    $form['resetSliders'] = [
      '#type' => 'details',
      '#title' => $this->t('Reset Sliders'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#open' => FALSE,
      '#tree' => TRUE,
    ];

    $form['resetSliders']['resetSlidersKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('resetSlidersKey'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => $config->get('resetSlidersKey'),
    ];

    $form['resetSliders']['resetSliders'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('resetSliders'),
      '#options' => [TRUE => $this->t('True'), FALSE => $this->t('False')],
      '#default_value' => $config->get('resetSliders'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('fullpagejs.settings')
      ->set('drupalContentTypes', $form_state->getValue('drupalContentTypes'))
      ->set('licenseKey', $form_state->getValue('licenseKey'))
      ->set('fullPageSelector', $form_state->getValue('fullPageSelector'))
      ->set('sectionSelector', $form_state->getValue('sectionSelector'))
      ->set('slideSelector', $form_state->getValue('slideSelector'))
      ->set('controlArrows', $form_state->getValue('controlArrows'))
      ->set('verticalCentered', $form_state->getValue('verticalCentered'))
      ->set('scrollingSpeed', $form_state->getValue('scrollingSpeed'))
      ->set('sectionsColor', $form_state->getValue('sectionsColor'))
      ->set('anchors', $form_state->getValue('anchors'))
      ->set('lockAnchors', $form_state->getValue('lockAnchors'))
      ->set('easing', $form_state->getValue('easing'))
      ->set('easingcss3', $form_state->getValue('easingcss3'))
      ->set('loopTop', $form_state->getValue('loopTop'))
      ->set('loopBottom', $form_state->getValue('loopBottom'))
      ->set('loopHorizontal', $form_state->getValue('loopHorizontal'))
      ->set('css3', $form_state->getValue('css3'))
      ->set('autoScrolling', $form_state->getValue('autoScrolling'))
      ->set('fitToSection', $form_state->getValue('fitToSection'))
      ->set('fitToSectionDelay', $form_state->getValue('fitToSectionDelay'))
      ->set('scrollBar', $form_state->getValue('scrollBar'))
      ->set('paddingTop', $form_state->getValue('paddingTop'))
      ->set('paddingBottom', $form_state->getValue('paddingBottom'))
      ->set('fixedElements', $form_state->getValue('fixedElements'))
      ->set('normalScrollElements', $form_state->getValue('normalScrollElements'))
      ->set('bigSectionsDestination', $form_state->getValue('bigSectionsDestination'))
      ->set('keyboardScrolling', $form_state->getValue('keyboardScrolling'))
      ->set('touchSensitivity', $form_state->getValue('touchSensitivity'))
      ->set('continuousVertical', $form_state->getValue('continuousVertical'))
      ->set('animateAnchor', $form_state->getValue('animateAnchor'))
      ->set('recordHistory', $form_state->getValue('recordHistory'))
      ->set('menu', $form_state->getValue('menu'))
      ->set('navigation', $form_state->getValue('navigation'))
      ->set('navigationPosition', $form_state->getValue('navigationPosition'))
      ->set('navigationTooltips', $form_state->getValue('navigationTooltips'))
      ->set('showActiveTooltip', $form_state->getValue('showActiveTooltip'))
      ->set('slidesNavigation', $form_state->getValue('slidesNavigation'))
      ->set('slidesNavPosition', $form_state->getValue('slidesNavPosition'))
      ->set('lazyLoading', $form_state->getValue('lazyLoading'))
      ->set('cards', $form_state->getValue(['cards', 'cards']))
      ->set('cardsKey', $form_state->getValue(['cards', 'cardsKey']))
      ->set('cardsPerspective', $form_state->getValue(['cards', 'cardsPerspective']))
      ->set('cardsFadeContent', $form_state->getValue(['cards', 'cardsFadeContent']))
      ->set('cardsFadeBackground', $form_state->getValue(['cards', 'cardsFadeBackground']))
      ->set('parallax', $form_state->getValue(['parallax', 'parallax']))
      ->set('parallaxKey', $form_state->getValue(['parallax', 'parallaxKey']))
      ->set('parallaxType', $form_state->getValue(['parallax', 'parallaxType']))
      ->set('parallaxPercentage', $form_state->getValue(['parallax', 'parallaxPercentage']))
      ->set('parallaxProperty', $form_state->getValue(['parallax', 'parallaxProperty']))
      ->set('scrollOverflow', $form_state->getValue('scrollOverflow'))
      ->set('scrollOverflowReset', $form_state->getValue(['scrollOverflowReset', 'scrollOverflowReset']))
      ->set('scrollOverflowResetKey', $form_state->getValue(['scrollOverflowReset', 'scrollOverflowResetKey']))
      ->set('dragAndMove', $form_state->getValue(['dragAndMove', 'dragAndMove']))
      ->set('dragAndMoveKey', $form_state->getValue(['dragAndMove', 'dragAndMoveKey']))
      ->set('offsetSections', $form_state->getValue(['offsetSections', 'offsetSections']))
      ->set('offsetSectionsKey', $form_state->getValue(['offsetSections', 'offsetSectionsKey']))
      ->set('fadingEffect', $form_state->getValue(['fadingEffect', 'fadingEffect']))
      ->set('fadingEffectKey', $form_state->getValue(['fadingEffect', 'fadingEffectKey']))
      ->set('responsiveWidth', $form_state->getValue('responsiveWidth'))
      ->set('responsiveHeight', $form_state->getValue('responsiveHeight'))
      ->set('responsiveSlides', $form_state->getValue(['responsiveSlides', 'responsiveSlides']))
      ->set('responsiveSlidesKey', $form_state->getValue(['responsiveSlides', 'responsiveSlidesKey']))
      ->set('continuousHorizontal', $form_state->getValue(['continuousHorizontal', 'continuousHorizontal']))
      ->set('continuousHorizontalKey', $form_state->getValue(['paracontinuousHorizontalllax', 'continuousHorizontalKey']))
      ->set('interlockedSlides', $form_state->getValue(['interlockedSlides', 'interlockedSlides']))
      ->set('interlockedSlidesKey', $form_state->getValue(['interlockedSlides', 'interlockedSlidesKey']))
      ->set('scrollHorizontally', $form_state->getValue(['scrollHorizontally', 'scrollHorizontally']))
      ->set('scrollHorizontallyKey', $form_state->getValue(['scrollHorizontally', 'scrollHorizontallyKey']))
      ->set('resetSliders', $form_state->getValue(['resetSliders', 'resetSliders']))
      ->set('resetSlidersKey', $form_state->getValue(['resetSliders', 'resetSlidersKey']))
      ->save();

    // Clear cache to update drupalSettings on script.js.
    drupal_flush_all_caches();
  }

}
